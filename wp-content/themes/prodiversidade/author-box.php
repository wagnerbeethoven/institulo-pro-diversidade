<?php if (get_the_author_meta('description')) : ?>
    <div class="author-box">
        <div class="author-img">
            <?php if (get_the_author_meta('url')) : ?>
                <a href="<?php esc_textarea(the_author_meta('url')); ?>" target="blank" title="Você irá abrir o site: <?php esc_textarea(the_author_meta('url')); ?>">
                <?php endif; ?>
                <?php echo get_avatar(get_the_author_meta('user_email'), '250'); ?>
                <?php if (get_the_author_meta('url')) : ?>
                </a>
            <?php endif; ?>
        </div>
        <div class="author-info">
            <h3 class="h3 gh author-dname author-name">
                <?php esc_html(the_author_meta('display_name')); ?>
            </h3>
            <hr>
            <p class="author-description">
                <?php esc_textarea(the_author_meta('description')); ?>
            </p>
            <p>

                <?php if (get_the_author_meta('url')) : ?>
                    <a href="<?php esc_textarea(the_author_meta('url')); ?>" target="blank" title="Você irá abrir o site: <?php esc_textarea(the_author_meta('url')); ?>">
                        <strong>
                            <?php esc_textarea(the_author_meta('url')); ?>
                        </strong>
                    </a> <br>
                <?php endif; ?>
                <a class="author-link" href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>" title="Veja todos os textos de <?php echo esc_attr(get_the_author()); ?>">
                    Veja todos os textos desse autor
                </a>

            </p>
        </div>
    </div>
<?php endif; ?>